import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { AuthStatus } from '../interfaces';

export const isAuthenticatedGuard: CanActivateFn = (route, state) => {
  // console.log('isAuthenticatedGuard');
  // console.log({route,state});
  // console.log(state.url);
  const authService=inject(AuthService);
  // console.log(authService.authStatus());
  if(authService.authStatus() ===AuthStatus.authenticated){
    return true;
  }

  if(authService.authStatus() ===AuthStatus.checking){
    return false;
  }

  const router= inject(Router);

  router.navigateByUrl('/auth/login');

  return false;
};
